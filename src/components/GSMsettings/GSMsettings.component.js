export default {
  name: 'gsmsettings',
  components: {}, 
  props: [],
  data () {
    return {
      valid: false,
      UserPassword: false,
      dailup: '',
      apn: '',
      user:'',
      password:'',
      pin:'',
        passRules: [],
        pinRules: [
          (v) => /^[0-9]{4,6}$/.test(v) || v ==='' || 'Pin must contain only digits'
        ],
        dailUpRules: [],
        apnRules: [],
        userRules: [],
        e1: true,
        e2: true
    }
  },
  computed: {

  },
  

  mounted () {

  },
  methods: {
    submit () {
      //this.$socket.emit('add', { name:"narek" });
      
      if (this.$refs.form.validate()) {
        this.$http.post(process.env.API_HOST+'/gsmsettings',{
          dailup: this.dailup,
          apn: this.apn,
          user:this.user,
          password:this.password,
          pin:this.pin,
          userPassword: this.UserPassword,
       },{
        headers: {
            Authorization: localStorage.token
        }
    }
     ).then(resp=>{
         console.log(resp.body)
       },resp=>{
         console.log("err")
       })
        
        this.$router.push({ path: '/dashboard/gsm' })

    
      }
    },
    clear () {
      this.$refs.form.reset()
    }
  }
}
