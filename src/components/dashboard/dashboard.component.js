export default {
  name: 'dashboard',
  components: {}, 
  props: [],
  data () {
    return {
      status:'Offline',
      vpnstatus:"off",
      snackbar:false,
      timeout:3000,
      text:"",
      drawer:false

    }
  },
  computed: {
    connectionStatus:function(){
      if(this.status==="Offline"){
        this.text=this.status
        this.snackbar=true
        return 'red accent-3'
      }else{
        this.text=this.status
        this.snackbar=true       
         
        return 'green accent-3'
      }

    },
    vpnStatus:function(){
      if(this.vpnstatus==="off"){
        return 'red'
      }else{
        return 'green'
      }
    }
  

  },
  socket: {
    events: {
      con(data){
        this.status=data.status;
        console.log(data.status)
      },
      vpn(data){
        this.vpnstatus=data.status;
        console.log(data.status)
      }
    }

  },
  mounted () {

  },
  methods: {
    page(dir){
      this.$router.push({ path: `/dashboard/${dir}` })
    },
    exit(){
      this.$router.push({ path: `/logout` })
  }
}
}
