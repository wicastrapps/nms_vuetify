export default {
  name: 'lansettings',
  components: {}, 
  props: [],
  data () {
    return {
      ip:/^(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$/,
      column:null,
      validStatic: false,
      validAuto: true,
      ipAdress: '',
      subnetMask: '',
      defaultGateway: '',
      primaryDNS: '',
      secondaryDNS: '',
      column:'auto',
      ipAdressRules: [
        (v) => this.ip.test(v) || v ==='' || 'Invalid'
      ],
      subnetMaskRules: [
        (v) => this.ip.test(v) || v ===''  || 'Invalid'
      ],
      defaultGatewayRules: [
        (v) => this.ip.test(v) || v ===''  || 'Invalid'
      ],
      primaryDNSRules: [
        (v) => this.ip.test(v) || v ===''  || 'Invalid'
      ],
      secondaryDNSRules: [
        (v) => this.ip.test(v) || v ===''  || 'Invalid'
      ]
      
    }
  },
  computed: {

  },
  mounted () {

  },
  methods: {
    submitManual () {
      if (this.$refs.form.validate()) {
        this.$http.post(process.env.API_HOST+'/lansettings',{
          ipAdress: this.ipAdress,
          subnetMask: this.subnetMask,
          defaultGateway: this.defaultGateway,
          primaryDNS: this.primaryDNS,
          secondaryDNS: this.secondaryDNS,
          settingsType:this.column
       },{
        headers: {
            Authorization: localStorage.token
        }
    }
     ).then(resp=>{
         console.log(resp.body)
       },resp=>{
         console.log("err")
       })
        this.$router.push({ path: '/dashboard/lan' })
      }
    },
    submitAuto () {
      this.$http.post(process.env.API_HOST+'/lansettings',{
          settingsType:this.column
       },{
        headers: {
            Authorization: localStorage.token
        }
    }
     ).then(resp=>{
         console.log(resp.body)
       },resp=>{
         console.log("err")
       })
        this.$router.push({ path: '/dashboard/lan' })

    },
    clear () {
      this.$refs.form.reset()
    }
  }
}
