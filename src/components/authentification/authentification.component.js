export default {
  name: 'authentification',
  components: {}, 
  props: [],
  data () {
    return {
        valid: false,
        name: 'admin',
        password: 'password',
        nameRules: [
          (v) => !!v || 'Name is required'
        ],
        passRules: [
          (v) => !!v || 'Password is required',
          (v) => v && v.length >= 8 || 'Password must be more than 8 characters'
        ],
        e1: true
    }
  },
  computed: {
    
  },
  mounted () {

  },

  methods: {
   submit () {
     
     //  this.$socket.emit("add", { name:"narek" });
      
   
        this.$http.post(process.env.API_HOST+'/auth',{
           name:this.name,
           password:this.password
        }
      ).then(resp=>{
          console.log(resp.body)
          localStorage.token=resp.body;
          this.$router.push({ path: '/dashboard/gsm' })
        },resp=>{
   
          console.log(resp)
          console.log("err")
        })
        // Native form submission is not yet supported
        // axios.post('/api/submit', {
        //   name: this.name,
        //   password: this.password
        // })

      }
    ,
    clear () {
      this.$refs.form.reset()
      this.$socket.emit('end');
    }

  }
}
