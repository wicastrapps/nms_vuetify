import Vue from 'vue'
import Vuex from 'vuex'
import vueResourse from 'vue-resource'

Vue.use(Vuex)
Vue.use(vueResourse)

const store = new Vuex.Store({
	state: {
        weather: {},
        token:""
		
	},
	getters: {
		jwt (state) {
			return state.token
		}
	},
	mutations: {
		set (state, { type, items }) {
			state[type] = items
		}
		
	},
	actions: {

		getWeather ({commit}, query) {
			const url = `${process.env.API_KEY}/today?city=${query}`
			Vue.http.get(url).then(
				response => {
					const results = response.body
					commit('set', { type: 'weather', items: results })
					commit('searchmarker')
                    commit('deleteError')
				},
				error => {
                    const message = error.body
					commit('setError', { error: message })
				}
			)
        },
        authorization({commit},payload){
            
            Vue.http.post('http://localhost:3001/auth',{
                name:payload.name,
                password:payload.password
            }
        ).then(
            response => {
                const result = response.body;
                //localStorage.token=result; 
                commit('set', { type: 'token', items: result })
                return new Promise(resolve=>{
                    localStorage.token=result;
                    resolve(); 
                })
                   
                },error => {
                    console.log(error.body)
                }
            )
        }

    }
})

export default store
