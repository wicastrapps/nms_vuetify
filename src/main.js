// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueResourse from 'vue-resource'
import store from './store/index'
import router from './router'
import Vuetify from 'vuetify'
import VueWebsocket from 'vue-websocket'

Vue.use(Vuetify)
Vue.use(VueResourse)
Vue.use(VueWebsocket, process.env.WS_HOST);

import 'vuetify/dist/vuetify.min.css'




Vue.config.productionTip = false

/* eslint-disable no-new */

// provide variables which can access globally with this.$var

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
