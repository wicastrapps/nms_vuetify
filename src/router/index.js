import Vue from 'vue'
import Router from 'vue-router'
import Authentification from '@/components/authentification'
import GSMsettings from '@/components/GSMsettings'
import LANsettings from '@/components/LANsettings'
import Dashboard from '@/components/dashboard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'authentification',
      component: Authentification,
      beforeEnter: (to, from, next) => {
        delete localStorage.token
        next()
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: requireAuth,
      
      children: [
        {
          path: '/dashboard/gsm',
          name: 'gsmsettings',
          component: GSMsettings
        },
        {
          path: '/dashboard/lan',
          name: 'lansettinds',
          component: LANsettings
        }
      ]
    },
    { path: '/logout',
    beforeEnter (to, from, next) {
      delete localStorage.token
      next('/')
    }
  }
    
  ]
})

function requireAuth (to, from, next) {
  if (!localStorage.token) {
    next({
      path: '/',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
}