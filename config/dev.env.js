'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  WS_HOST:'"http://10.10.10.120:3000"',
  API_HOST:'"http://10.10.10.120"'
})
